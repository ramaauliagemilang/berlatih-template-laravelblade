@extends('layout.master')
@section('judul')
Halaman Register
@endsection

@section('isi')
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post">
        @csrf
        <label> First name: </label> <br>
        <input type="text" name="first name"> <br><br>
        <label> Last name: </label> <br>
        <input type="text" name="last name"> <br><br>
        <label> Gender: </label> <br>
        <input type="radio" name="Gender" > Male <br>
        <input type="radio" name="Gender" > Female <br>
        <input type="radio" name="Gender" > Other <br><br>
        <label> Nationality: </label> 
        <select name="National">
            <option value="indonesian"> Indonesian </option>
            <option value="singaporean"> Singaporean </option>
            <option value="malaysian"> Malaysian </option>
            <option value="Australian"> Thailand </option>
        </select> <br><br>
        <label> Language Spoken: <l/abel> <br><br>
        <input type="checkbox" name="bahasa indonesia" > Bahasa Indonesia <br>
        <input type="checkbox" name="english" > English <br>
        <input type="checkbox" name="arabic" > Arabic <br>
        <input type="checkbox" name="japanese" > Japanese <br><br>
        <label> Bio: </label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection



